#ifndef DIO_H
#define DIO_H

#include "DIO_cfg.h"

extern void DIO_init(void);
extern void DIO_writePin(DIO_pinName_t pinName, u8 value);
extern u8 DIO_readPin(DIO_pinName_t pinName);

#endif