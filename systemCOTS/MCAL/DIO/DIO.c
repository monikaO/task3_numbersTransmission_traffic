#include "STD_TYPES.h"
#include "DIO.h"

typedef enum
{
  PORTA = *((u8*)0x3B),
  PORTB = *((u8*)0x38),
  PORTC = *((u8*)0x35),
  PORTD = *((u8*)0x32)
}DIO_PortRegisters_t;

typedef enum 
{
  DDRA = *((u8*)0x3A),
  DDRB = *((u8*)0x37),
  DDRC = *((u8*)0x34),
  DDRD = *((u8*)0x31)
}DIO_DDRRegisters_t;

typedef enum
{
  PINA = *((u8*)0x39),
  PINB = *((u8*)0x36),
  PINC = *((u8*)0x33),
  PIND = *((u8*)0x30)
}DIO_PINRegisters_t;

extern void DIO_init(void)
{
  u8 loopIndex;
  
  for(loopIndex=0;loopIndex<sizeof(DIO_cfg)/sizeof(DIO_cfg_t);loopIndex++)
  {
    DIO_DDRRegisters_t DDR;
    u8 portNumber = DIO_cfg[loopIndex].pinName / 8;
    u8 pinNumber = DIO_cfg[loopIndex].pinName % 8;
    switch(portNumber)
    {
      case 0:
              DDR = DDRA;
              break;
      case 1:
              DDR = DDRB;
              break;
      case 2:
              DDR = DDRC;
              break;
      default:
              DDR = DDRD;
    }
    if(DIO_cfg[loopIndex].mode == Output)
    {
      /* set bit */
      DDR|= 1<<pinNumber;
    }
    else if(DIO_cfg[loopIndex].mode == InputWIPUR)
    {
      /* clr bit */
      DDR&=~(1<<pinNumber);
                
      /* initiate pull up */
      DIO_writePin(DIO_cfg[loopIndex].pinName,1);
    }
    else 
    {
      /* clr bit */
      DDR&=~(1<<pinNumber);
    }
  }
}
extern void DIO_writePin(DIO_pinName_t pinName, u8 value)
{
  DIO_PortRegisters_t port;
  u8 portNumber = pinName / 8;
  u8 pinNumber = pinName % 8;
  switch(portNumber)
  {
    case 0:
            port = PORTA;
            break;
    case 1:
            port = PORTB;
            break;
    case 2:
            port = PORTC;
            break;
    default:
            port = PORTD;
            
  }
  if(value == 1)
  {
    /* set bit */
    port|=1<<pinNumber;
  }
  else
  {
    /* clr bit */
    port&=~(1<<pinNumber);
  }
}
extern u8 DIO_readPin(DIO_pinName_t pinName)
{
  DIO_PINRegisters_t pin;
  u8 portNumber = pinName / 8;
  u8 pinNumber = pinName % 8;
  switch(portNumber)
  {
    case 0 :
             pin = PINA;
             break;
    case 1:
             pin = PINB;
             break;
    case 2:
             pin = PINC;
             break;
    default:
             pin = PIND;
  }
  return (pin>>pinNumber)&1;
}