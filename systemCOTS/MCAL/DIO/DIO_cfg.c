#include "DIO_cfg.h"

const DIO_cfg_t DIO_cfg[] =
{
  {LED1_traffic,Output,highLevel}, //PA0
  {LED2_traffic,Output,highLevel}, //PA1
  {LED3_traffic,Output,highLevel}, //PA2
  {LED1_status,Output,highLevel},  //PA3
  {LED2_status,Output,highLevel},  //PA4
  {SEG1,Output,highLevel},         //PA5
  {SEG2,Output,highLevel},         //PA6
  {PA7,Output,highLevel},          //PA7
  {SEG1A,Output,lowLevel},         //PB0
  {SEG1B,Output,lowLevel},         //PB1
  {SEG1C,Output,lowLevel},         //PB2
  {SEG1D,Output,lowLevel},         //PB3
  {SEG1E,Output,lowLevel},         //PB4
  {SEG1F,Output,lowLevel},         //PB5
  {SEG1G,Output,lowLevel},         //PB6
  {SEG1DOT,Output,lowLevel},       //PB7
  {SEG2A,Output,lowLevel},         //PC0
  {SEG2B,Output,lowLevel},         //PC1
  {SEG2C,Output,lowLevel},         //PC2
  {SEG2D,Output,lowLevel},         //PC3 
  {SEG2E,Output,lowLevel},         //PC4
  {SEG2F,Output,lowLevel},         //PC5
  {SEG2G,Output,lowLevel},         //PC6
  {SEG2DOT,Output,lowLevel},       //PC7
  {PD0,Output,highLevel},          //PD0
  {PD1,Output,highLevel},          //PD1
  {PD2,Output,highLevel},          //PD2
  {PD3,Output,highLevel},          //PD3
  {PD4,Output,highLevel},          //PD4
  {PD5,Output,highLevel},          //PD5
  {PD6,Output,highLevel},          //PD6
  {PD7,Output,highLevel}           //PD7
}

