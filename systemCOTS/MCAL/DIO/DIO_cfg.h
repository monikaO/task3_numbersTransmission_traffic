#ifndef DIO_CFG_H
#define DIO_CFG_H

typedef enum
{
  LED1_traffic=0, LED2_traffic, LED3_traffic, LED1_status, LED2_status, SEG1, SEG2, PA7,
  SEG1A, SEG1B, SEG1C, SEG1D, SEG1E, SEG1F, SEG1G, SEG1DOT,
  SEG2A, SEG2B, SEG2C, SEG2D, SEG2E, SEG2F, SEG2G, SEG2DOT,
  PD0, PD1, PD2, PD3, PD4, PD5, PD6, PD7
}DIO_pinName_t;

typedef enum
{
  InputWIPUR =0,
  InputWOPUR,
  Output
}DIO_mode_t;

typedef enum
{
  lowLevel =0,
  highLevel
}DIO_activationLevel_t;

typedef struct
{
  DIO_pinName_t pinName;
  DIO_mode_t mode;
  DIO_activationLevel_t activationLevel;
}DIO_cfg_t;

extern const DIO_cfg_t DIO_cfg[];

#endif