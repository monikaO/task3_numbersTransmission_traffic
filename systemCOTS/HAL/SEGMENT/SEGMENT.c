#include "STD_TYPES.h"

#include "DIO.h"

#include "SEGMENT.h"

const u8 sevSegDataAnode[]={0b11000000,
                            0b11001111,
                            0b10100100,
                            0b10110000,
                            0b10011001,
                            0b10010010,
                            0b10000010,
                            0b11111000,
                            0b10000000,
                            0b10010000,
                            0b00001000,
                            0b00000000,
                            0b11000110,
                            0b01000000,
                            0b10000110,
                            0b10001110,
                            0b11111111};
					 
const u8 sevSegDataCathode[]={0b00111111,
                              0b00110000,
                              0b01011011,
                              0b01001111,
                              0b01100110,
                              0b01101101,
                              0b01111101,
                              0b00000111,
                              0b01111111,
                              0b01101111,
                              0b11110111,
                              0b11111111,
                              0b00111001,
                              0b10111111,
                              0b01111001,
                              0b01110001,
                              0b00000000};
        
/* This function is used to enable the seven segment									*/
void SEGMENT_voidEnable(DIO_pinName_t segmentName)
{
  if(DIO_cfg[segmentName].activationLevel == lowLevel) // common cathode
  {
    DIO_writePin(segmentName,0);
  }
  else // common anode
  {
    DIO_writePin(segmentName,1);
  }
}

/* This function is used to disable the seven segment									*/
void SEGMENT_voidDisable(DIO_pinName_t segmentName)
{
  if(DIO_cfg[segmentName].activationLevel == lowLevel) // common cathode
  {
    DIO_writePin(segmentName,1);
  }
  else // common anode
  {
    DIO_writePin(segmentName,0);
  }
  
}

/* This function is used to output a specific number on the seven segment				*/
void SEGMENT_voidDisplayNumber(u8 u8NumberCpy, DIO_pinName_t segmentName)
{
  /*define local variable to hold the current processing bit value*/
	u8 u8BitValueLOC;
  
  if(DIO_cfg[segmentName].activationLevel == lowLevel) // common cathode
  {
    /*update u8NumberCpy to be equal its real value from the lookup table*/
    u8NumberCpy=sevSegDataCathode[u8NumberCpy];
    u8BitValueLOC = (u8NumberCpy>>0)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1A,1);
      }
      else
      {
        DIO_writePin(SEG2A,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1A,0);
      }
      else
      {
        DIO_writePin(SEG2A,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>1)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1B,1);
      }
      else
      {
        DIO_writePin(SEG2B,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1B,0);
      }
      else
      {
        DIO_writePin(SEG2B,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>2)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1C,1);
      }
      else
      {
        DIO_writePin(SEG2C,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1C,0);
      }
      else
      {
        DIO_writePin(SEG2C,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>3)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1D,1);
      }
      else
      {
        DIO_writePin(SEG2D,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1D,0);
      }
      else
      {
        DIO_writePin(SEG2D,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>4)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1E,1);
      }
      else
      {
        DIO_writePin(SEG2E,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1E,0);
      }
      else
      {
        DIO_writePin(SEG2E,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>5)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1F,1);
      }
      else
      {
        DIO_writePin(SEG2F,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1F,0);
      }
      else
      {
        DIO_writePin(SEG2F,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>6)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1G,1);
      }
      else
      {
        DIO_writePin(SEG2G,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1G,0);
      }
      else
      {
        DIO_writePin(SEG2G,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>7)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1DOT,1);
      }
      else
      {
        DIO_writePin(SEG2DOT,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1DOT,0);
      }
      else
      {
        DIO_writePin(SEG2DOT,0);
      }
    }
    
  }
  else // common anode
  {
    /*update u8NumberCpy to be equal its real value from the lookup table*/
    u8NumberCpy=sevSegDataAnode[u8NumberCpy];
    u8BitValueLOC = (u8NumberCpy>>0)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1A,1);
      }
      else
      {
        DIO_writePin(SEG2A,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1A,0);
      }
      else
      {
        DIO_writePin(SEG2A,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>1)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1B,1);
      }
      else
      {
        DIO_writePin(SEG2B,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1B,0);
      }
      else
      {
        DIO_writePin(SEG2B,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>2)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1C,1);
      }
      else
      {
        DIO_writePin(SEG2C,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1C,0);
      }
      else
      {
        DIO_writePin(SEG2C,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>3)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1D,1);
      }
      else
      {
        DIO_writePin(SEG2D,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1D,0);
      }
      else
      {
        DIO_writePin(SEG2D,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>4)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1E,1);
      }
      else
      {
        DIO_writePin(SEG2E,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1E,0);
      }
      else
      {
        DIO_writePin(SEG2E,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>5)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1F,1);
      }
      else
      {
        DIO_writePin(SEG2F,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1F,0);
      }
      else
      {
        DIO_writePin(SEG2F,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>6)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1G,1);
      }
      else
      {
        DIO_writePin(SEG2G,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1G,0);
      }
      else
      {
        DIO_writePin(SEG2G,0);
      }
    }
    
    u8BitValueLOC = (u8NumberCpy>>7)&1;
    if(u8BitValueLOC==1)
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1DOT,1);
      }
      else
      {
        DIO_writePin(SEG2DOT,1);
      }
    }
    else 
    {
      if(segmentName==SEG1)
      {  
        DIO_writePin(SEG1DOT,0);
      }
      else
      {
        DIO_writePin(SEG2DOT,0);
      }
    }
  }
}