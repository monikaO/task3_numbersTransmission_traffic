#ifndef SEGMENT_H
#define SEGMENT_H

/* This function is used to enable the seven segment									*/
extern void SEGMENT_voidEnable(DIO_pinName_t segmentName);

/* This function is used to disable the seven segment									*/
extern void SEGMENT_voidDisable(DIO_pinName_t segmentName);

/* This function is used to output a specific number on the seven segment				*/
extern void SEGMENT_voidDisplayNumber(u8 u8NumberCpy, DIO_pinName_t segmentName);

#endif