#include "STD_TYPES.h"

#include "DIO.h"

#include "LED.h"

/****************************************/
/* this function turns a certain LED on */
/* depending on the sent name */
/****************************************/
extern void LED_voidSetLedOn(DIO_pinName_t ledName)
{
  if(DIO_cfg[ledName].activationLevel == lowLevel) // reverse mode
  {
    DIO_writePin(ledName, 0);
  }
  else // normal mode
  {
    DIO_writePin(ledName, 1);
  }
 
}

/****************************************/
/* this function turns a certain LED off */
/* depending on the sent name */
/****************************************/
extern void LED_voidSetLedOff(DIO_pinName_t ledName)
{
  if(DIO_cfg[ledName].activationLevel == lowLevel) // reverse mode
  {
    DIO_writePin(ledName, 1);
  }
  else // normal mode
  {
    DIO_writePin(ledName, 0);
  }
}
