#ifndef LED_H
#define LED_H

/****************************************/
/* this function turns a certain LED on */
/* depending on the sent name */
/****************************************/
extern void LED_voidSetLedOn(DIO_pinName_t ledName);

/****************************************/
/* this function turns a certain LED off */
/* depending on the sent name */
/****************************************/
extern void LED_voidSetLedOff(DIO_pinName_t ledName);

#endif